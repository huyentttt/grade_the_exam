
#Task 1:

import numpy as np
answer_key = "B,A,D,D,C,B,D,A,C,C,D,B,A,B,A,C,B,D,A,C,A,A,B,D,D".strip().split()
number_answer = len(answer_key)

def check_file(filename):
    print("\n Checking file: *******************************\n")
    filename = input("Enter a filename: ")
    with open(filename):
        try: 
            print("Open file successfully!")
            return True
        except IOError: 
            print("Error opening file")
            return False
        
   # Task 2:
        
def check_student(code):
    if(code[0] != N):
        return False
    elif(len(code) != 9):
        return False
    for char in code[1:]:
        if not char.isdigit():
            return False
    return True

def analyze(filename):
    print("Analyzing******\n" )
    with open(filename, 'rb') as f_obj:
        total_lines = 0
        valid_lines = 0
        ass_dict = dict()
        
        for line in f_obj.readlines():
            result = line.strip().split(",")
            if len(result) -1 != number_answer:
                print("Invalid line- Incorrect number answer")
                return result
            elif not check_student(result[0]):
                print("Invalid data _ Incorrect student code")
                return result
            else:
                valid_lines += 1
                ass_dict[result[0]] =result[1:]
            total_lines += 1
            
            
        if valid_lines == total_lines:
            print("No err founds")
        print("\n Report: +++++++++++++++ \n")
        print("total_lines of data: {}".format(total_lines))
        print("total valid lines of data: {}".format(valid_lines))
        print("Total invalid lines of data: {}".format(total_lines - valid_lines))
        
        if valid_lines == 0:
            return 0
    return ass_dict

# Task 3: 

def grade(ass_dict):
    print("Grade: ******************************************")
    for student, result in ass_dict.items():
        
        score = 0
        for i in range(len(result)):
            if result[i] == answer_key[i]:
                score += 4
            elif result[i] == "":
                pass
            else: 
                score += -1
        ass_dict[student] = score
        
        ##Output:
    score = np.array(list(ass_dict.values()))
	print("Number of students graded:".ljust(30, "-")
		  + " {}".format(len(ass_dict)))
	print("Average score:".ljust(30, "-")
		  + " {:.2f}".format(score.mean()))
	print("Highest score:".ljust(30, "-")
		  + " {}".format(score.max()))
	print("Lowest score:".ljust(30, "-")
		  + " {}".format(score.min()))
	print("Range of scores:".ljust(30, "-")
		  + " {}".format(score.max() - score.min()))
	print("Median value:".ljust(30, "-")
		  + " {:.2f}".format(np.median(score)))

	# Return the dict with student:score pairs
	return ass_dict

# Task 4:
def write_file(scores_dict: dict, filename: str):
	"""
    Writes result scores into a file
    :param scores_dict: dict contains student code and score as key:value
    :param filename: original name of the file that was used for grading
    """
	print("\n" + "EXPORTING RESULTS!".center(30, "-") + "\n")
	filename = filename.replace(".txt", "_grades.txt")
	with open(filename, "w+") as file_object:
		for key, value in+ scores_dict.items():
			file_object.write(key + "," + str(value) + "\n")
	print("Results saved to '{}' successfully!".format(filename))


while True:
	file = input("Input your filename: ")
	if check_file(file):
		assignments = analyze(file)
		if assignments:
			scores = grade(assignments)
			write_file(scores, file)
		else:
			print("No valid line to grade!")
	print("\n" + "RESTARTING!".center(30, "-") + "\n")
